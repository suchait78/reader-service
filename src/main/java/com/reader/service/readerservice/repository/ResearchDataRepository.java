package com.reader.service.readerservice.repository;

import com.reader.service.readerservice.model.ResearchData;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface ResearchDataRepository extends SolrCrudRepository<ResearchData, String> {
}
