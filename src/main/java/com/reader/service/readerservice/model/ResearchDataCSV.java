package com.reader.service.readerservice.model;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResearchDataCSV {

        private String fips;
        private String admin2;
        private String provinceState;
        private String countryRegion;
        private String lastUpdate;
        private Double lat;
        private Double long_;
        private String confirmed;
        private String deaths;
        private String recovered;
        private String active;
        private String combinedKey;
        private Double incidentRate;
        private Double caseFatalityRatio;

        public static class ResearchDataCSVBuilder {

                private String fips;
                private String admin2;
                private String provinceState;
                private String countryRegion;
                private String lastUpdate;
                private Double lat;
                private Double long_;
                private String confirmed;
                private String deaths;
                private String recovered;
                private String active;
                private String combinedKey;
                private Double incidentRate;
                private Double caseFatalityRatio;

                public ResearchDataCSVBuilder withFips(String fips) {
                        this.fips = fips;
                        return this;
                }

                public ResearchDataCSVBuilder withAdmin2(String admin2) {
                        this.admin2 = admin2;
                        return this;
                }

                public ResearchDataCSVBuilder withProvinceState(String provinceState) {
                        this.provinceState = provinceState;
                        return this;
                }

                public ResearchDataCSVBuilder withCountryRegion(String countryRegion) {
                        this.countryRegion = countryRegion;
                        return this;
                }

                public ResearchDataCSVBuilder withLastUpdate(String lastUpdate) {
                        this.lastUpdate = lastUpdate;
                        return this;
                }

                public ResearchDataCSVBuilder withLat(Double lat) {
                        this.lat = lat;
                        return this;
                }

                public ResearchDataCSVBuilder withLong_(Double long_) {
                        this.long_ = long_;
                        return this;
                }

                public ResearchDataCSVBuilder withConfirmed(String confirmed) {
                        this.confirmed = confirmed;
                        return this;
                }

                public ResearchDataCSVBuilder withDeaths(String deaths) {
                        this.deaths = deaths;
                        return this;
                }

                public ResearchDataCSVBuilder withRecovered(String recovered) {
                        this.recovered = recovered;
                        return this;
                }

                public ResearchDataCSVBuilder withActive(String active) {
                        this.active = active;
                        return this;
                }

                public ResearchDataCSVBuilder withCombinedKey(String combinedKey) {
                        this.combinedKey = combinedKey;
                        return this;
                }

                public ResearchDataCSVBuilder withIncidentRate(Double incidentRate) {
                        this.incidentRate = incidentRate;
                        return this;
                }

                public ResearchDataCSVBuilder withCaseFatalityRatio(Double caseFatalityRatio) {
                        this.caseFatalityRatio = caseFatalityRatio;
                        return this;
                }

                public ResearchData build() {
                        ResearchData researchData = new ResearchData();
                        researchData.setFips(this.fips);
                        researchData.setActive(this.active);
                        researchData.setAdmin2(this.admin2);
                        researchData.setCountryRegion(this.countryRegion);
                        researchData.setConfirmed(this.confirmed);
                        researchData.setDeaths(this.deaths);
                        researchData.setIncidentRate(this.incidentRate);
                        researchData.setLastUpdate(this.lastUpdate);
                        researchData.setLatitude(this.lat);
                        researchData.setLongitude(this.long_);
                        researchData.setCaseFatalityRatio(this.caseFatalityRatio);
                        researchData.setProvinceState(this.provinceState);
                        researchData.setRecovered(this.recovered);
                        researchData.setCombinedKey(this.combinedKey);
                        return researchData;
                }
        }

}
