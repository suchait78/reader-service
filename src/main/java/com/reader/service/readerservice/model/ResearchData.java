package com.reader.service.readerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SolrDocument(collection = "research")
public class ResearchData {

    @Id
    @Indexed(name = "id", type = "string")
    private String id;

    @Indexed(name = "fips", type = "string")
    private String fips;

    @Indexed(name = "admin2", type = "string")
    private String admin2;

    @Indexed(name = "province_state", type = "string")
    private String provinceState;

    @Indexed(name = "country_region", type = "string")
    private String countryRegion;

    @Indexed(name = "last_update", type = "string")
    private String lastUpdate;

    @Indexed(name = "latitude", type = "string")
    private Double latitude;

    @Indexed(name = "longitude", type = "string")
    private Double longitude;

    @Indexed(name = "confirmed", type = "string")
    private String confirmed;

    @Indexed(name = "deaths", type = "string")
    private String deaths;

    @Indexed(name = "recovered", type = "string")
    private String recovered;

    @Indexed(name = "active", type = "string")
    private String active;

    @Indexed(name = "combined_key", type = "string")
    private String combinedKey;

    @Indexed(name = "incident_rate", type = "string")
    private Double incidentRate;

    @Indexed(name = "case_fatality_ratio", type = "string")
    private Double caseFatalityRatio;

}
