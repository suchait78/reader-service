package com.reader.service.readerservice.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SampleService {

    public void sampleMethod() {
        log.info("SampleService called.");
    }
}
