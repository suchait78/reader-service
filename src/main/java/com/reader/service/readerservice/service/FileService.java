package com.reader.service.readerservice.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface FileService {

    public void download(String url, String localFilename) throws IOException, ExecutionException, InterruptedException;
}
