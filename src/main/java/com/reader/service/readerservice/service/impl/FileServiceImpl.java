package com.reader.service.readerservice.service.impl;

import com.reader.service.readerservice.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.asynchttpclient.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class FileServiceImpl implements FileService {

    @Value("${download.github.url}")
    private String downloadUrl;

    public String createFullUrl() {

        //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        //LocalDateTime now = LocalDateTime.now();
        //String finalUrl = downloadUrl.concat(dtf.format(now));

        String finalUrl = downloadUrl.concat("02-24-2021.csv");

        log.info("URL generated : {} ", finalUrl);

        return finalUrl;
    }

    @Override
    public void download(String url, String filename)
            throws IOException, ExecutionException, InterruptedException {

        FileOutputStream stream = new FileOutputStream(filename);
        AsyncHttpClient client = Dsl.asyncHttpClient();

        client.prepareGet(url)
                .execute(new AsyncCompletionHandler<FileOutputStream>() {

                    @Override
                    public State onBodyPartReceived(HttpResponseBodyPart bodyPart) throws Exception {
                        stream.getChannel()
                                .write(bodyPart.getBodyByteBuffer());
                        return State.CONTINUE;
                    }

                    @Override
                    public FileOutputStream onCompleted(Response response) throws Exception {
                        return stream;
                    }
                })
                .get();

        stream.getChannel().close();
        client.close();

    }

}
