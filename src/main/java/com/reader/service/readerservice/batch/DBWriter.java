package com.reader.service.readerservice.batch;

import com.reader.service.readerservice.model.ResearchData;
import com.reader.service.readerservice.model.ResearchDataCSV;
import com.reader.service.readerservice.repository.ResearchDataRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DBWriter implements ItemWriter<ResearchDataCSV> {

    @Autowired
    private ResearchDataRepository researchDataRepository;

    private ResearchData createResearchData(ResearchDataCSV researchDataCSV) {
        return new ResearchDataCSV.ResearchDataCSVBuilder()
                .withFips(researchDataCSV.getFips())
                .withActive(researchDataCSV.getActive())
                .withAdmin2(researchDataCSV.getAdmin2())
                .withCaseFatalityRatio(researchDataCSV.getCaseFatalityRatio())
                .withCombinedKey(researchDataCSV.getCombinedKey())
                .withConfirmed(researchDataCSV.getConfirmed())
                .withCountryRegion(researchDataCSV.getCountryRegion())
                .withDeaths(researchDataCSV.getDeaths())
                .withIncidentRate(researchDataCSV.getIncidentRate())
                .withLastUpdate(researchDataCSV.getLastUpdate())
                .withLat(researchDataCSV.getLat())
                .withLong_(researchDataCSV.getLong_())
                .withProvinceState(researchDataCSV.getProvinceState())
                .withRecovered(researchDataCSV.getRecovered())
                .build();

    }

    @Override
    public void write(List<? extends ResearchDataCSV> researchDataCSVList) throws Exception {

        //TODO : This is just for testing code , will remove later
        AtomicInteger pos = new AtomicInteger();

        researchDataCSVList.forEach(element -> {
            log.info(String.valueOf(researchDataCSVList.get(0)));
            pos.getAndIncrement();
        });

        log.info("total size : {}", researchDataCSVList.size());
        //TODO: Ends here

        List<ResearchData> resultResearchDataList =
        researchDataCSVList.stream()
                .map(researchDataCSV -> createResearchData(researchDataCSV))
                .collect(Collectors.toList());

        researchDataRepository.saveAll(resultResearchDataList);

        log.info("data has been stored in solr.");

    }
}
