package com.reader.service.readerservice.batch;

import com.reader.service.readerservice.model.ResearchDataCSV;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Processor implements ItemProcessor<ResearchDataCSV, ResearchDataCSV> {

    @Override
    public ResearchDataCSV process(ResearchDataCSV researchDataCSV) throws Exception {
        return researchDataCSV;
    }
}
