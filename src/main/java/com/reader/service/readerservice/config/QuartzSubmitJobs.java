package com.reader.service.readerservice.config;

import com.reader.service.readerservice.job.ReaderJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
public class QuartzSubmitJobs {

    //TODO : take this from properties file
    private static final String CRON_EVERY_FIVE_MINUTES = "  0 0/1 * 1/1 * ? *  ";

    @Bean(name = "sampleServiceJobDetail")
    public JobDetailFactoryBean jobMemberStats() {
        return QuartzConfig.createJobDetail(ReaderJob.class, "Sample Service Job");
    }


    @Bean(name = "sampleServiceTrigger")
    public CronTriggerFactoryBean triggerMemberClassStats(@Qualifier("sampleServiceJobDetail") JobDetail jobDetail) {
        return QuartzConfig.createCronTrigger(jobDetail, CRON_EVERY_FIVE_MINUTES, "Class Statistics Trigger");
    }
}
