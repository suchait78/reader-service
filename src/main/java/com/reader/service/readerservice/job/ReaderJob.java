package com.reader.service.readerservice.job;

import com.reader.service.readerservice.service.SampleService;
import com.reader.service.readerservice.service.impl.FileServiceImpl;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@DisallowConcurrentExecution
public class ReaderJob implements Job {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    org.springframework.batch.core.Job job;

    @Autowired
    private FileServiceImpl fileService;

    @Value("${local.filename}")
    private String localFilename;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        log.info("job execute method called.");

        this.fileService.download(this.fileService.createFullUrl(), this.localFilename);
        log.info("File downloaded successfully.");

        log.info("Calling batch operation now.");
        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(job, parameters);

    }
}
